import os
import configparser
from SourceCode import userinterface
from SourceCode.FindingManager import FindingManager
from SourceCode import log


def main():
    """
        Main-Function
    """
    # DEMOSW-58: This is a finding
    # This is the description

    # Load config file
    config = configparser.ConfigParser()
    config.read(os.path.join(os.path.abspath(__file__), "..\SourceCode\config.ini"))

    # Parse console arguments
    args = userinterface.parseArguments(config)
    log.initialize_logger(os.path.join(os.path.abspath(__file__), ".."), args.debug)
    try:
        log.start()

        # Change extension string to set
        extensions = args.extensions.replace(" ", "")
        extensions = set((extensions.split(",")))
        if args.python:
            extensions = set(("py",))

        # Start script
        manager = FindingManager(args.path, extensions, args.python, config)
        manager.findings2Jira()

        log.stop()
    except Exception as e:
        log.error("Anything was wrong!", e)


# All top-level code must be contained in main() function. This prevents
# the execution of this code when the module is imported by another one.
# When called using 'python xxx.py', main() is executed automatically.
if __name__ == "__main__":
    main()
