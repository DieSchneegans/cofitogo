// DEMOSW-51: (5) Finding with defined id
//	This finding-id can find on jira server.

// pre-processor value to enable a switch. For feature switches, prefer FS_ENABLE.
#define STD_ON		1

// pre-processor value to disable a switch. For feature switches, prefer FS_DISABLE.
#define STD_OFF 	0

// DEMOSW-50: (6) classical finding-placeholder
//	this
// is
// 	    	a
//				finding
//	with
//       many
//	description			lines.

// pre-processor value to enable a switch. For feature switches, prefer FS_ENABLE.
#define STD_ON		1

// pre-processor value to disable a switch. For feature switches, prefer FS_DISABLE.
#define STD_OFF 	0

// DEMOSW-135: (7) finding with defined id
//	finding-id not on server.

// pre-processor value to enable a switch. For feature switches, prefer FS_ENABLE.
#define STD_ON		1

// pre-processor value to disable a switch. For feature switches, prefer FS_DISABLE.
#define STD_OFF 	0
