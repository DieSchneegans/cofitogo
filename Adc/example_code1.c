// DEMOSW-xxx: Finding without description

// pre-processor value to enable a switch. For feature switches, prefer FS_ENABLE.
#define STD_ON		1

// pre-processor value to disable a switch. For feature switches, prefer FS_DISABLE.
#define STD_OFF 	0

// DEMOSW-29: (2) finding-id-placeholder with more X
//	This finding have eight x in placeholder.

// pre-processor value to enable a switch. For feature switches, prefer FS_ENABLE.
#define STD_ON		1

// pre-processor value to disable a switch. For feature switches, prefer FS_DISABLE.
#define STD_OFF 	0

// DEMOSW-35: (3) finding with less x
//	This finding have two x
//	and a second line of description.

// pre-processor value to enable a switch. For feature switches, prefer FS_ENABLE.
#define STD_ON		1

// pre-processor value to disable a switch. For feature switches, prefer FS_DISABLE.
#define STD_OFF 	0

// DEMOSW-39: (4) finding with no free line between code and finding-comment
//	This finding have no line between code and finding.
//	The finding-id-placeholder have 4 digits.
// pre-processor value to enable a switch. For feature switches, prefer FS_ENABLE.
#define STD_ON		1

// pre-processor value to disable a switch. For feature switches, prefer FS_DISABLE.
#define STD_OFF 	0

// DEMOSW-XXXX:
//	This finding have no line between code and finding.
//	The finding-id-placeholder have 4 digits.
// And no summary!
// pre-processor value to enable a switch. For feature switches, prefer FS_ENABLE.
#define STD_ON		1

// pre-processor value to disable a switch. For feature switches, prefer FS_DISABLE.
#define STD_OFF 	0

// DEMOSW-33: (8) Now with summary
//	The finding-id-placeholder have 4 digits.
// And 		now 	     with 		summary!

// pre-processor value to enable a switch. For feature switches, prefer FS_ENABLE.
#define STD_ON		1

// pre-processor value to disable a switch. For feature switches, prefer FS_DISABLE.
#define STD_OFF 	0
