# System imports
import pytest
import builtins
import getpass
import jira
from pathlib import Path
import subprocess
import os
import configparser

# Module imports
from SourceCode import FindingManager as FM
from SourceCode import SourceBase as SB


# MOCKS -----------------------------------------------------


class Mock_SourceBase:
    def __init__(self):
        self.__rootPath = ""
        self.__files = [MockSourceFile(), MockSourceFile()]

    def getFiles(self):
        return self.__files


class MockSourceFile:
    def __init__(self):
        self.__filePath = ""
        self.__fileText = "Dies ist der Inhalt der Datei!"
        self.__findings = [MockFinding(), MockFinding()]

    def getFindings(self):
        return self.__findings


class MockFinding:
    def __init__(self):
        self.__project = "TEST"
        self.__id = "XXXX"
        self.__posStart = 0
        self.__summary = "SUMMARY"
        self.__description = "DESCRIPTION"
        self.__issueType = {"name": "Finding"}
        self.__components = [{"name": "AFFE"}]

    def getSummary(self):
        return self.__summary

    def getPositionStart(self):
        return self.__posStart

    def setId(self, id):
        self.__id = id

    def getId(self):
        return self.__id

    def getProject(self):
        return self.__project

    def getIsDefined(self):
        return None

    def toDict(self):
        return {
            "project": {"key": "TEST"},
            "summary": "Testsummary",
            "description": "Testdescription",
            "issuetype": {"name": "Finding"},
            "components": [{"name": "ToolDev"}],
        }


def mock_SourceBase(*args, **kwargs):
    return Mock_SourceBase()


class Mock_Jira:
    def __init__(self):
        self.server = ""
        self.basic_auth = []

    def create_issue(self, fields):
        return MockIssue()

    def issue(self):
        return MockIssue()


class MockIssue:
    def __init__(self):
        self.key = "TEST-123"


def mock_jira(*args, **kwargs):
    return Mock_Jira()


def mock_input(*args, **kwargs):
    return "y"


def mock_getpass(*args, **kwargs):
    return "132"


def mock_console(*args, **kwargs):
    f = open("console.log", "w")
    f.write(args[0])


# FIXTURE -----------------------------------------------------
extensions = {"c", "h"}
path = Path("C:\Projects\Internal2.0\BSW\ToolDev\FindingsToJira")
silent = False


def configFile():
    config = configparser.ConfigParser()
    config.read(os.path.join(os.path.abspath(__file__), "../../SourceCode/config.ini"))
    return config


@pytest.fixture
def FindingManager(monkeypatch):
    monkeypatch.setattr(SB, "SourceBase", mock_SourceBase)
    monkeypatch.setattr(jira, "JIRA", mock_jira)
    monkeypatch.setattr(builtins, "input", mock_input)
    monkeypatch.setattr(getpass, "getpass", mock_getpass)
    monkeypatch.setattr(subprocess, "Popen", mock_console)

    config = configFile()
    return FM.FindingManager(path, extensions, silent, config)


# TESTCASE 20, 21 ---------------------------------------------
@pytest.mark.FindingManager
def test_findings2Jira(FindingManager):
    FindingManager.findings2Jira()
    f = open("console.log", "r")
    assert (
        f.read()
        == 'tortoiseproc /command:commit /path:C:\Projects\Internal2.0\BSW\ToolDev\FindingsToJira /logmsg:"TEST-<Id>: Registered findings to jira. Ids: TEST-123, TEST-123, TEST-123, TEST-123"'
    )
