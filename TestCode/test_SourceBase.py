# System imports
import pytest
import yaml
import builtins
import io
import os
from pathlib import Path
import configparser

# Module imports
from SourceCode import SourceBase as SB
from SourceCode import SourceFile as SF


# MOCKS -----------------------------------------------------
def mock_safeLoad(*args, **kwargs):
    return {"FolderBlacklist": ["ExampleCode", "MotM", "SysM"]}


def mock_open(*args, **kwargs):
    return io.TextIOBase()


class MockSourceFile:
    def __init__(self):
        self.__filePath = ""
        self.__fileText = "Dies ist der Inhalt der Datei!"
        self.__findings = []


def mock_SourceFile(*args, **kwargs):
    return MockSourceFile()


# FIXTURE -----------------------------------------------------
extensions = {"c", "h"}
path = Path("C:\Projects\Internal2.0\BSW\ToolDev\CoFiToGo")


def configFile():
    config = configparser.ConfigParser()
    config.read(os.path.join(os.path.abspath(__file__), "../../SourceCode/config.ini"))
    return config


@pytest.fixture
def SourceBase(monkeypatch):
    monkeypatch.setattr(yaml, "safe_load", mock_safeLoad)
    monkeypatch.setattr(builtins, "open", mock_open)
    monkeypatch.setattr(SF, "SourceFile", mock_SourceFile)
    config = configFile()
    return SB.SourceBase(path, extensions, config)


# TESTCASE 18, 19 ---------------------------------------------
@pytest.mark.SourceBase
def test_getFiles(SourceBase):
    assert len(SourceBase.getFiles()) == 2 and type(SourceBase.getFiles()) == list
