# System imports
import pytest
import builtins
import io
import os
import configparser

# Module imports
from SourceCode import SourceFile as SF
from SourceCode import Finding as F


# MOCKS -----------------------------------------------------
fileText = """// DEMOSW-XXX: (1) Finding with finding-id-placeholder
//    The finding-id-placeholder have correct structure. Only 4 XXXX.

// pre-processor value to enable a switch. For feature switches, prefer FS_ENABLE.
#define STD_ON        1

// pre-processor value to disable a switch. For feature switches, prefer FS_DISABLE.
#define STD_OFF     0

// DEMOSW-XXX: (2) finding-id-placeholder with more X
//    This finding have eight x in placeholder.

// pre-processor value to enable a switch. For feature switches, prefer FS_ENABLE.
#define STD_ON        1

// pre-processor value to disable a switch. For feature switches, prefer FS_DISABLE.
#define STD_OFF     0"""


def mock_open(*args, **kwargs):
    return io.TextIOBase()


def mock_read(*args, **kwargs):
    return fileText


def mock_close(*args, **kwargs):
    pass


def mock_write(*args, **kwargs):
    return fileText


class MockFinding:
    def __init__(self):
        self.__project = "TEST"
        self.__id = "XXXX"
        self.__posStart = 340
        self.__summary = "SUMMARY"
        self.__description = "DESCRIPTION"
        self.__issueType = {"name": "Finding"}
        self.__components = [{"name": "AFFE"}]

    def getSummary(self):
        return self.__summary

    def getPositionStart(self):
        return self.__posStart

    def setId(self, id):
        self.__id = id


def mock_Finding(*args, **kwargs):
    return MockFinding()


# FIXTURE ---------------------------------------------------
filePath = os.path.abspath(
    "C:\Projects\Internal2.0\BSW\ToolDev\CoFiToGo\Adc\example_code1.c"
)


def configFile():
    config = configparser.ConfigParser()
    config.read(os.path.join(os.path.abspath(__file__), "../../SourceCode/config.ini"))
    return config


@pytest.fixture
def SourceFile(monkeypatch):
    config = configFile()

    monkeypatch.setattr(builtins, "open", mock_open)
    monkeypatch.setattr(io.TextIOBase, "read", mock_read)
    monkeypatch.setattr(io.TextIOBase, "close", mock_close)
    monkeypatch.setattr(io.TextIOBase, "write", mock_write)
    monkeypatch.setattr(F, "Finding", mock_Finding)

    return SF.SourceFile(filePath, config)


# TESTCASE 14 -------------------------------------------------
@pytest.mark.SourceFile
def test_fileRead(SourceFile):
    assert SourceFile._SourceFile__fileText == fileText


# TESTCASE 15 -------------------------------------------------
@pytest.mark.SourceFile
def test_parseFindings(SourceFile):
    assert len(SourceFile._SourceFile__findings) == 2


# TESTCASE 16 -------------------------------------------------
@pytest.mark.SourceFile
def test_setFindingID(SourceFile):
    SourceFile.setFindingID(
        SourceFile._SourceFile__findings[1], "42"
    )  # Finding 1 - posStart=0; Finding 2 - posStart=340
    assert (
        SourceFile._SourceFile__fileText
        == """// DEMOSW-XXX: (1) Finding with finding-id-placeholder
//    The finding-id-placeholder have correct structure. Only 4 XXXX.

// pre-processor value to enable a switch. For feature switches, prefer FS_ENABLE.
#define STD_ON        1

// pre-processor value to disable a switch. For feature switches, prefer FS_DISABLE.
#define STD_OFF     0

// DEMOSW-42: (2) finding-id-placeholder with more X
//    This finding have eight x in placeholder.

// pre-processor value to enable a switch. For feature switches, prefer FS_ENABLE.
#define STD_ON        1

// pre-processor value to disable a switch. For feature switches, prefer FS_DISABLE.
#define STD_OFF     0"""
    )


# TESTCASE 17 -------------------------------------------------
@pytest.mark.SourceFile
def test_getFindings(SourceFile):
    assert (
        SourceFile.getFindings() == SourceFile._SourceFile__findings
        and type(SourceFile.getFindings()) == list
    )
