# System imports
import pytest
import os
import configparser

# Module imports
from SourceCode import Finding as F


# FIXTURE -----------------------------------------------------
def configFile():
    config = configparser.ConfigParser()
    config.read(os.path.join(os.path.abspath(__file__), "../../SourceCode/config.ini"))
    return config


@pytest.fixture
def Finding(p_text, p_pos, p_path):
    config = configFile()
    return F.Finding(p_text, p_pos, p_path, config)


# TESTCASE 01, 02 ---------------------------------------------
@pytest.mark.Finding
@pytest.mark.parametrize(
    "p_text, p_pos, p_path, p_project",
    [
        (
            """// AFFE-123: This is a summary \n//    This is a description """,
            0,
            os.path.join(os.path.abspath("."), "test.c"),
            "AFFE",
        ),
        (
            """// TEST-42: Testsummary \n//    Testdescription """,
            112,
            os.path.join(os.path.abspath("."), "test.c"),
            "TEST",
        ),
    ],
)
def test_getProject(Finding, p_text, p_pos, p_path, p_project):
    assert Finding.getProject() == p_project and type(Finding.getProject()) == str


# TESTCASE 03, 04 ---------------------------------------------
@pytest.mark.Finding
@pytest.mark.parametrize(
    "p_text, p_pos, p_path, p_id",
    [
        (
            """// AFFE-123: This is a summary \n//    This is a description """,
            0,
            os.path.join(os.path.abspath("."), "test.c"),
            "123",
        ),
        (
            """// TEST-42: Testsummary \n//    Testdescription """,
            112,
            os.path.join(os.path.abspath("."), "test.c"),
            "42",
        ),
    ],
)
def test_getId(Finding, p_text, p_pos, p_path, p_id):
    assert Finding.getId() == p_id and type(Finding.getId()) == str


# TESTCASE 05, 06 ---------------------------------------------
@pytest.mark.Finding
@pytest.mark.parametrize(
    "p_text, p_pos, p_path",
    [
        # Var 1 - Pos is 0 (int)
        (
            """// AFFE-123: This is a summary \n//    This is a description """,
            0,
            os.path.join(os.path.abspath("."), "test.c"),
        ),
        # Var 2 - Pos is 112 (int)
        (
            """// TEST-42: Testsummary \n//    Testdescription """,
            112,
            os.path.join(os.path.abspath("."), "test.c"),
        ),
        # Var 3 - Pos is 112 (str)
        (
            """// TEST-42: Testsummary \n//    Testdescription """,
            "112",
            os.path.join(os.path.abspath("."), "test.c"),
        ),
        # Var 4 - Pos is set(112)
        (
            """// TEST-42: Testsummary \n//    Testdescription """,
            set(("112")),
            os.path.join(os.path.abspath("."), "test.c"),
        ),
        # Var 5 - Pos is tuple(112)
        (
            """// TEST-42: Testsummary \n//    Testdescription """,
            tuple(("112")),
            os.path.join(os.path.abspath("."), "test.c"),
        ),
        # Var 6 - Pos is dict(112)
        (
            """// TEST-42: Testsummary \n//    Testdescription """,
            dict([("pos", 0)]),
            os.path.join(os.path.abspath("."), "test.c"),
        ),
        # Var 7 - Pos is list(112)
        (
            """// TEST-42: Testsummary \n//    Testdescription """,
            list(("112")),
            os.path.join(os.path.abspath("."), "test.c"),
        ),
    ],
)
def test_getPositionStart(Finding, p_text, p_pos, p_path):
    if type(p_pos) == int:
        assert (
            Finding.getPositionStart() == p_pos
            and type(Finding.getPositionStart()) == int
        )
    else:
        assert Finding.getPositionStart() != p_pos


# TESTCASE 07, 08 ---------------------------------------------
@pytest.mark.Finding
@pytest.mark.parametrize(
    "p_text, p_pos, p_path, p_summary",
    [
        (
            """// AFFE-123: This is a summary \n//    This is a description """,
            0,
            os.path.join(os.path.abspath("."), "test.c"),
            "This is a summary",
        ),
        (
            """// TEST-42: Testsummary \n//    Testdescription """,
            112,
            os.path.join(os.path.abspath("."), "test.c"),
            "Testsummary",
        ),
    ],
)
def test_getSummary(Finding, p_text, p_pos, p_path, p_summary):
    assert Finding.getSummary() == p_summary and type(Finding.getSummary()) == str


# TESTCASE 09, 10 ---------------------------------------------
@pytest.mark.Finding
@pytest.mark.parametrize(
    "p_text, p_pos, p_path, p_description",
    [
        (
            """// AFFE-123: This is a summary \n//    This is a description """,
            0,
            os.path.join(os.path.abspath("."), "test.c"),
            "This is a description",
        ),
        (
            """// TEST-42: Testsummary \n//    Testdescription """,
            112,
            os.path.join(os.path.abspath("."), "test.c"),
            "Testdescription",
        ),
    ],
)
def test_getDescription(Finding, p_text, p_pos, p_path, p_description):
    assert (
        Finding._Finding__description == p_description
        and type(Finding._Finding__description) == str
    )


# TESTCASE 11 -------------------------------------------------
@pytest.mark.Finding
@pytest.mark.parametrize(
    "p_text, p_pos, p_path, p_project, p_summary, p_description",
    [
        (
            """// AFFE-123: This is a summary \n//    This is a description """,
            0,
            os.path.join(os.path.abspath("."), "test.c"),
            "AFFE",
            "This is a summary",
            "This is a description",
        ),
        (
            """// TEST-42: Testsummary \n//    Testdescription """,
            112,
            os.path.join(os.path.abspath("."), "test.c"),
            "TEST",
            "Testsummary",
            "Testdescription",
        ),
    ],
)
def test_toDict(Finding, p_text, p_pos, p_path, p_project, p_summary, p_description):
    assert Finding.toDict() == {
        "project": {"key": p_project},
        "summary": p_summary,
        "description": p_description,
        "issuetype": {"name": "Finding"},
        "components": [{"name": "Temp"}],
    }


# TESTCASE 12, 13 ---------------------------------------------
@pytest.mark.Finding
@pytest.mark.parametrize(
    "p_text, p_pos, p_path, p_id",
    [
        (
            """// AFFE-123: This is a summary \n//    This is a description """,
            0,
            os.path.join(os.path.abspath("."), "test.c"),
            123,
        ),
        (
            """// TEST-42: Testsummary \n//    Testdescription """,
            112,
            os.path.join(os.path.abspath("."), "test.c"),
            "42",
        ),
        (
            """// TEST-42: Testsummary \n//    Testdescription """,
            112,
            os.path.join(os.path.abspath("."), "test.c"),
            set(("112")),
        ),
        (
            """// TEST-42: Testsummary \n//    Testdescription """,
            112,
            os.path.join(os.path.abspath("."), "test.c"),
            tuple(("112")),
        ),
        (
            """// TEST-42: Testsummary \n//    Testdescription """,
            112,
            os.path.join(os.path.abspath("."), "test.c"),
            dict([("id", 0)]),
        ),
        (
            """// TEST-42: Testsummary \n//    Testdescription """,
            112,
            os.path.join(os.path.abspath("."), "test.c"),
            list(("112")),
        ),
        (
            """// TEST-42: Testsummary \n//    Testdescription """,
            112,
            os.path.join(os.path.abspath("."), "test.c"),
            1,
        ),
    ],
)
def test_setId(Finding, p_text, p_pos, p_path, p_id):
    Finding.setId(p_id)
    if type(p_id) == int or type(p_id) == str:
        assert Finding._Finding__id == str(p_id) and type(Finding._Finding__id) == str
    else:
        assert Finding._Finding__id != p_id


# TESTCASE 22, 23 ---------------------------------------------
@pytest.mark.Finding
@pytest.mark.parametrize(
    "p_text, p_pos, p_path",
    [
        (
            """// AFFE-123: This is a summary \n//    This is a description """,
            0,
            os.path.join(os.path.abspath("."), "test.c"),
        ),
        (
            """// TEST-42: Testsummary \n//    Testdescription """,
            112,
            os.path.join(os.path.abspath("."), "test.c"),
        ),
    ],
)
def test_parseComponent(Finding, p_text, p_pos, p_path):
    assert (
        Finding._Finding__components == {"name": "Temp"}
        and type(Finding._Finding__components) == dict
    )
