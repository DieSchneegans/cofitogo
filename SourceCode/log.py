import logging
import os.path


def initialize_logger(output_dir, mode_debug):
    """
        Initialize logger -> Logging to console and logging to files
        Source: https://aykutakin.wordpress.com/2013/08/06/\
        logging-to-console-and-file-in-python/
    """
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)

    # activate debug logging mode
    if mode_debug:
        mode = logging.DEBUG
    else:
        mode = logging.INFO

    # create console handler and set level to info
    handler = logging.StreamHandler()
    handler.setLevel(logging.WARNING)
    formatter = logging.Formatter("%(levelname)s - %(message)s")
    handler.setFormatter(formatter)
    logger.addHandler(handler)

    # create error file handler and set level to error
    handler = logging.FileHandler(
        os.path.join(output_dir, "error.log"), "w", encoding=None, delay="true"
    )
    handler.setLevel(logging.WARNING)
    formatter = logging.Formatter(
        "%(asctime)s [%(levelname)s] %(message)s", datefmt="%d %b %Y %H:%M:%S"
    )
    handler.setFormatter(formatter)
    logger.addHandler(handler)

    # create info/debug file handler and set level to debug
    handler = logging.FileHandler(os.path.join(output_dir, "all.log"), "w")
    handler.setLevel(mode)
    formatter = logging.Formatter(
        "%(asctime)s [%(levelname)s] %(message)s", datefmt="%d %b %Y %H:%M:%S"
    )
    handler.setFormatter(formatter)
    logger.addHandler(handler)


def findingIdOnServer(findingId):
    """
        Logging:
        The defined finding with finding-id %s
        has been found on jira server
    """
    logging.info(
        "The defined finding with finding-id %s has been found on jira server",
        findingId,
    )


def findingIdNotOnServer(findingId):
    """
        Logging:
        The defined finding with finding-id %s has not
        been found on jira server
    """
    logging.warning(
        "The defined finding with finding-id %s has not been found on jira server",
        findingId,
    )


def findingSummaryNotEqual(findingId):
    """
        Logging:
        The summary of source code finding %s
        is not equal with Jira finding summary
    """
    logging.warning(
        "The summary of source code finding %s is not equal with Jira finding summary",
        findingId,
    )


def findingDescriptionNotEqual(findingId):
    """
        Logging:
        The description of source code finding %s
        is not equal with Jira finding description
    """
    logging.warning(
        "The description of source code finding %s is not equal with Jira finding description",
        findingId,
    )


def newFindingIssue(findingId, dataFields):
    """
        Logging:
        A new finding-issue was create on jira server.
        The finding-id is %s.
        The fields have been filled with the following data: %s
    """
    logging.info(
        "A new finding-issue was create on jira server. The finding-id is %s. The fields have been filled with the following data: %s",
        findingId,
        dataFields,
    )


def updatedFindingIssue(findingId, dataFields):
    """
        Logging:
        The finding with the finding-id %s is now updated.
        The fields have been filled with the following data: %s
    """
    logging.info(
        "The finding with the finding-id %s is now updated. The fields have been filled with the following data: %s",
        findingId,
        dataFields,
    )


def replaceFindingId(findingId, filePath):
    """
        Logging:
        The finding-id %s of finding was replaced in the read-in file (%s)
    """
    logging.info(
        "The finding-id %s of finding was replaced in the read-in file (%s)",
        findingId,
        filePath,
    )


def foundFinding(filePath):
    """
        Logging:
        A new finding was found into file (%s).
    """
    logging.info("A new finding was found into file (%s).", filePath)


def error(txt, exception):
    """
        Logging:
        txt - exception
    """
    logging.error("%s - Exception: %s", txt, exception)


def debug_txt(txt):
    """
        Logging:
        txt
    """
    logging.debug(txt)


def noSummary(filePath, position):
    """
        Logging:
        No summary found for the finding (Startposition: %s) in file (%s).
        Finding is not longer treated.
    """
    logging.warning(
        "No summary found for the finding (Startposition: %s) in file (%s). Finding is not longer treated.",
        position,
        filePath,
    )


def folderBlacklist(folderName, dirpath):
    """
        Logging:
        A folder was blacklisted and skipped.
    """
    logging.warning(
        "The folder %s and all subfolder was blacklisted and skipped. (%s)",
        folderName,
        dirpath,
    )


def debug(exception):
    """
        Logging:
        Exception as debug
    """
    logging.debug("%s", exception)


def start():
    """
        Logging:
        Script is running...
    """
    logging.info("Script is running...")


def stop():
    """
        Logging:
        Script is stopped
    """
    logging.info("Script is stopped")
