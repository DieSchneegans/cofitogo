import subprocess
import re
import jira_mod as jira
from SourceCode import SourceBase
from SourceCode import userinterface
from SourceCode import log
from jira_mod.exceptions import JIRAError


class FindingManager:
    """
        Manage the connection between local (folder system)
        and online (jira server)
    """

    def __init__(self, sourceRoot, fileExtensions, modePython, configFile):
        """
           Constructor - Create an object of class FindingManager
           and save object information
        """

        # Top folder path for searching
        self.__folderPath = sourceRoot

        # file endings by searching files
        self.__extensions = fileExtensions

        # python mode
        self.__modePython = modePython

        # config
        self.__config = configFile

    def findings2Jira(self):
        """
            Search findings and push it to jira server
        """
        # Create SourceBase object
        # -> Search all files with specific extensions
        # and create for each an SourceFile object
        # -> Search all findings into file
        # and create for each an Finding object
        sourceBaseObject = SourceBase.SourceBase(
            self.__folderPath, self.__extensions, self.__config, self.__modePython
        )
        sourceBaseObject.searchFiles()

        # Get all findings
        files = self.__getFiles(sourceBaseObject)

        # Close script if no files founded
        if files == []:
            log.error(
                "No files found!",
                "No files with extensions {} found in folders.".format(
                    self.__extensions
                ),
            )
            return

        # show all findings on console
        countFindings = self.__showFindingsOnConsole(files)

        # stop scipt if no findings found
        if countFindings == 0:
            return

        # ask user to continue
        if not userinterface.userInputList(
            "\nPlease check the findings! \
            \nLook up for defined finding-issues on jira server and create undefined finding-issues on jira server? [y/n] ",
            ["1", "y", "Y", "yes"],
        ):
            return

        # Enter Jira username
        emptyInputUsername = 0
        while emptyInputUsername < 3:
            emptyInputUsername += 1
            inputUsername = userinterface.userInputTextWithPromt(
                "Enter your Jira username: "
            )
            if inputUsername != "":
                break
            elif inputUsername == "" and emptyInputUsername != 3:
                print("No username! Please try it again...")
        else:
            log.error(
                "No username!",
                "Your input is empty. If you want to look up for defined finding issues and create undefined finding issues your Jira username is required!",
            )
            return

        # Enter Jira password
        emptyInputPassword = 0
        while emptyInputPassword < 3:
            emptyInputPassword += 1
            inputPassword = userinterface.userInputTextWithoutPromt(
                "Enter your Jira password: "
            )
            if inputPassword != "":
                break
            elif inputPassword == "" and emptyInputPassword != 3:
                print("No password! Please try it again...")
        else:
            log.error(
                "No password!",
                "Your input is empty. If you want to look up for defined finding issues and create undefined finding issues your Jira password is required!",
            )
            return
        print("\n")

        # Connect to jira server
        jiraServer = self.__createJiraConnection(inputUsername, inputPassword)

        if jiraServer == JIRAError:
            return

        # Create issues or compare finding
        # and update source code with finding-id
        issueIDs = []
        for sourceFile in files:
            findings = sourceFile.getFindings()
            findings.reverse()
            for finding in findings:
                # is finding defined or undefined
                if finding.getIsDefined():
                    # Try getting issue
                    issue = self.__getJiraIssue(jiraServer, finding)
                else:
                    try:
                        # Create finding-issue if finding undefined
                        issue = self.__createJiraIssue(jiraServer, finding)

                        # Get finding-id from jira server
                        findingId = re.sub(".*-", "", issue.key)
                        issueIDs.append(issue.key)

                        # Get dataFields
                        dataFields = finding.toDict()

                        # Logging
                        log.newFindingIssue(findingId, dataFields)

                        # Replace finding-id into source file
                        sourceFile.setFindingID(finding, findingId)
                        sourceFile.writeFindingToFile()

                    except Exception as e:
                        log.error(
                            "Error in the process of creating and accepting a finding issue",
                            e,
                        )
        else:
            # Output - Successful transfer to jira server
            # and change ids into files
            userinterface.successfulTransfer(issueIDs)

            if issueIDs == []:
                print("Nothing was changed! No commit required.")
                return
            else:
                print("Please commit the updated jira keys in your sources!")
            # SVN Commit
            project = re.search(
                "({})-({})".format(
                    self.__config["finding-pattern"]["project"],
                    self.__config["finding-pattern"]["validnumber"],
                ),
                issueIDs[0],
            ).group(1)
            subprocess.Popen(
                'tortoiseproc /command:commit /path:{} /logmsg:"{}-<Id>: Registered findings to jira. Ids: {}"'.format(
                    self.__folderPath, project, ", ".join(issueIDs)
                )
            )

    def __getFiles(self, sourceBaseObject):
        """
            Get all sourceFiles and return it into a list
        """
        return sourceBaseObject.getFiles()

    def __showFindingsOnConsole(self, files):
        """
            Generate a console output with all findings
            and show this on console
        """

        # Counter findings
        countFindings = 0
        countFindingDefined = 0

        # Get all findings
        for sourceFile in files:
            findings = sourceFile.getFindings()
            for finding in findings:
                countFindings += 1
                findingInfos = finding.toDict()
                findingId = finding.getId()
                if finding.getIsDefined():
                    countFindingDefined += 1
                    findingState = "check"
                else:
                    findingState = "create"

                outputFinding = "Finding {} ({})\
                \n\tJira-Key:\t{}-{}\
                \n\tSummary:\t{}\
                \n\tDescription:\t{}\
                \n\tComponents:\t{}"

                if "components" not in findingInfos.keys():
                    components = ""
                else:
                    components = findingInfos["components"][0]["name"]

                print(
                    outputFinding.format(
                        countFindings,
                        findingState,
                        findingInfos["project"]["key"],
                        findingId,
                        findingInfos["summary"],
                        findingInfos["description"],
                        components,
                    )
                )
        else:
            if countFindings == 0:
                print("No findings found!")
                return countFindings
            else:
                print(
                    "\n\n{} findings found...\
                    \n{} findings are defined (with numerical id)...\
                    \n{} undefined findings to creating on jira server...".format(
                        countFindings,
                        countFindingDefined,
                        countFindings - countFindingDefined,
                    )
                )

    def __createJiraConnection(self, username, password):
        """
            Open an connection to jira server
            Connection info: HTTP Basic Authentication
        """

        # Server address
        server = self.__config["default"]["url"]

        # Connection to server
        try:
            jiraServer = jira.JIRA(
                server,
                basic_auth=(username, password),
                get_server_info=False,
                max_retries=0,
                timeout=3,
                trust_env=False,
            )
        except JIRAError as e:
            errorMessagePos = re.search("<title>.+</title>", e.text)
            errorMessage = e.text[
                errorMessagePos.start() + 7 : errorMessagePos.end() - 8
            ]
            if errorMessage == "Unauthorized (401)":
                log.error(
                    "Jira denied access. Password correct?",
                    "JiraError HTTP url: {}/rest/api/2/serverInfo - {}".format(
                        self.__config["default"]["url"], errorMessage
                    ),
                )
                return JIRAError
            elif errorMessage == "502 Bad Gateway":
                log.error(
                    "A bad gateway error is not a happy error!",
                    "JiraError HTTP url: {}/rest/api/2/serverInfo - {}".format(
                        self.__config["default"]["url"], errorMessage
                    ),
                )
                return JIRAError
            else:
                log.error("Can not open connection to jira!", e)
                return JIRAError

        return jiraServer

    def __createJiraIssue(self, jiraServer, findingObject):
        """
            Create finding-issue on jira server
        """

        try:
            return jiraServer.create_issue(fields=findingObject.toDict())
        except Exception as e:
            log.error(
                "Can not create finding-issue {} on jira server".format(
                    findingObject.getProject() + "-" + findingObject.getId()
                ),
                e,
            )
            return None

    def __getJiraIssue(self, jiraServer, findingObject):
        """
            Get an issue object with specific id
        """

        # Get finding-id
        project = findingObject.getProject()
        id = findingObject.getId()
        summary = findingObject.getSummary()
        description = findingObject.getDescription()
        findingId = project + "-" + id

        # Get finding-issue from jira server
        try:
            issue = jiraServer.issue(findingId)
            log.findingIdOnServer(findingId)
            if summary != issue.fields.summary:
                log.findingSummaryNotEqual(findingId)
            if description != issue.fields.description:
                log.findingDescriptionNotEqual(findingId)
        except Exception as e:
            issue = {}
            log.findingIdNotOnServer(findingId)
            log.debug(e)

        return issue
