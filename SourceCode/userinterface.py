import argparse as argparse
import stdiomask

EPILOG = """\
Example usage:
python cofitogo.py C:\Projects\Internal2.0\BSW\ToolDev\CoFiToGo\Adc;
"""

DESCRIPTION = """\
This script create Jira finding issues after a code review automatically.
Call this script with a folder path to find all findings in files matching extensions
and create these findings in Jira. The script replace the finding-id-placeholder in source
with Jira finding-id.
"""

HELP_PATH = """\
Specifies folder tree to search for files with specific extensions.
May or may not end with a forward slash.
"""

HELP_EXTENSION = """\
Use the parameter to define all file extensions with findings.
Structure: comma seperated string
Default: '{}'
"""

HELP_PYTHON = """\
Choose python mode.
In this case the component is empty and the script choose the extension "py".
Default: False
"""

HELP_DEBUG = """\
Activate debug logging mode.
Default: False
"""


def parseArguments(configFile):
    """
        Generate arguments by calling script
    """

    # Create ArgumentParser
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawTextHelpFormatter,
        description=DESCRIPTION,
        epilog=EPILOG,
    )

    # Add arguments
    parser.add_argument("path", type=str, help=HELP_PATH)
    parser.add_argument(
        "-e",
        "--extensions",
        type=str,
        help=HELP_EXTENSION.format(configFile["default"]["extension"]),
        default=configFile["default"]["extension"],
    )
    parser.add_argument("-p", "--python", action="store_true", help=HELP_PYTHON)
    parser.add_argument("-d", "--debug", action="store_true", help=HELP_DEBUG)

    return parser.parse_args()


def userInputList(txt, trueOptions):
    """
        Generate a userInput from console with special text and options.
        param: trueOptions - list of strings
    """
    return input(txt) in trueOptions


def userInputTextWithPromt(txt):
    """
        Generate a userInput from console with special text and options.
        param: txt - text to show
    """
    return input(txt)


def userInputTextWithoutPromt(txt):
    """
        Generate a userInput from console with special text and options.
        param: txt - text to show
    """
    return stdiomask.getpass(txt)


def successfulTransfer(issueIDs):
    """
        Console output without line in log: Successful transfer of data to jira server and change id-placeholder into files.
    """
    issues = ""
    for issueId in issueIDs:
        issues += "\t" + issueId + "\n"

    if issues == "":
        print(
            "\n\nSuccessful! All data transfered to jira server. Please check the warnings above this message!"
        )
    else:
        print(
            "\n\nSuccessful! All data transfered to jira server. Please check the warnings above this message! \
            \nNew finding-issues:\n{} \nThe finding-id-placeholder changed with issue-id into source code files.".format(
                issues
            )
        )
