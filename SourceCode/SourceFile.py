import re
from SourceCode import Finding
from SourceCode import log


class SourceFile:
    """
        Do all file specific operations and search information into it.
    """

    def __init__(self, filePath, configFile, modePython):
        """
            Constructor - Create an object of class SourceFile
            and save object information
        """
        # Save file path
        self.__filePath = filePath

        # Config
        self.__config = configFile

        # Python mode
        self.__modePython = modePython

        # List for all objects of Finding
        self.__findings = []

    def readFileText(self):
        """
            Open and read a file
        """
        file = self.__open("r")
        self.__fileText = self.__read(file)
        self.__close(file)
        del file

    def __open(self, mode):
        """
            Open a file and return this
        """
        return open(self.__filePath, mode)

    def __read(self, file):
        """
            Get complete text from a file
        """
        return file.read()

    def __write(self, file, newText):
        """
            Overwrite a text into file
        """
        file.write(newText)

    def __close(self, file):
        """
            Close opened file
        """
        file.close()

    def parseFindings(self):
        """
            Search findings into text of opened file
        """
        # Get all findings with finding-id or finding-id-placeholder
        fileFindings = re.finditer(
            "{}\s*({})-({}|{})\s*:".format(
                self.__config["file-extension-comment"][self.__filePath.split(".")[-1]],
                self.__config["finding-pattern"]["project"],
                self.__config["finding-pattern"]["validnumber"],
                self.__config["finding-pattern"]["placeholder"],
            ),
            self.__fileText,
        )

        # Create a object of Finding and push into finding list
        for fileFinding in fileFindings:
            # Search position of finding
            findingStart = fileFinding.start()
            findingEnd = re.search(
                "\n[\w#]|\n\s*\n", self.__fileText[findingStart:]
            ).start()

            # Create finding object
            finding = Finding.Finding(
                self.__fileText[findingStart : findingStart + findingEnd],
                fileFinding.start(),
                self.__filePath,
                self.__config,
                self.__modePython,
            )

            if finding.getSummary() != "" and finding.getPositionStart != "":
                # Add finding to list
                self.__findings.append(finding)

                # Logging
                log.foundFinding(self.__filePath)

    def setFindingID(self, finding, findingId):
        """
            Overwrite finding-id-placeholder with finding-id (from jira server)
        """
        # Overwrite in file text finding-id at finding position
        posFindingStart = finding.getPositionStart()

        textFindingId = re.search(
            "-({}):".format(self.__config["finding-pattern"]["placeholder"]),
            self.__fileText[posFindingStart:],
        )
        finding.setId(findingId)
        self.__fileText = (
            self.__fileText[: posFindingStart + textFindingId.start() + 1]
            + str(findingId)
            + self.__fileText[posFindingStart + textFindingId.end() - 1 :]
        )

        # Logging
        log.replaceFindingId(findingId, self.__filePath)

    def writeFindingToFile(self):
        """
            Write saved file text to file with saved file path
        """
        # Open file in mode overwrite
        file = self.__open("w")

        # Overwrite file text
        self.__write(file, self.__fileText)

        # Close file
        self.__close(file)

    def getFindings(self):
        """
            Return all findings in a list
        """
        return self.__findings
