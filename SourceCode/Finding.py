import re
import os
from SourceCode import log


class Finding:
    """
        Save all information of a finding
        and identify the information in source code from file
    """

    def __init__(self, findingText, findingStart, filePath, configFile, modePython):
        """
            Constructor - Create an object of class Finding
            and parses all finding information
        """

        # Config
        self.__config = configFile

        # Python mode
        self.__modePython = modePython

        # file path
        self.__filePath = filePath

        # All finding objects has issue type "Finding"
        self.__issueType = {"name": self.__config["finding-pattern"]["issueType"]}

        # start position of finding object into source file text
        if type(findingStart) != int:
            self.__posStart = ""
            return
        self.__posStart = findingStart

        # Parse project
        self.__project = self.__parseProject(findingText)

        # Parse id
        self.__id = self.__parseId(findingText)
        self.__isDefined = self.__isDefinedFinding()

        # Parse summary
        self.__summary = self.__parseSummary(findingText)

        # Logging empty summary
        if self.__summary == "":
            log.noSummary(filePath, self.__posStart)
            return

        # Parse description
        self.__description = self.__parseDescription(findingText)

        # Parse components
        self.__components = self.__parseComponents()

    def __parseProject(self, findingText):
        """
            Parse project by using finding text
            project = text between comment-sign (//) and minus (-)
            e.g.: DEMOSW
        """

        # Search project into finding text
        return re.search(
            "{}\s*({})-".format(
                self.__config["file-extension-comment"][self.__filePath.split(".")[-1]],
                self.__config["finding-pattern"]["project"],
            ),
            findingText,
        ).group(1)

    def __parseId(self, findingText):
        """
            Parse Id by using finding text
            id = numbers between minus (-) and colon (:)
            e.g.: 213
        """

        # Search id-placeholder or id into finding text
        return re.search(
            "-({}|{})\s*:".format(
                self.__config["finding-pattern"]["validnumber"],
                self.__config["finding-pattern"]["placeholder"],
            ),
            findingText,
        ).group(1)

    def __parseSummary(self, findingText):
        """
            Parse summary by using finding text and delete whitespaces in front
            and end of text.

            summary = end of finding-id (:) -> end of line
        """

        # Start position = first position after finding-id
        # or finding-id-placeholder
        summary_start = re.search(
            "{}\s*({})-({}|{})\s*:".format(
                self.__config["file-extension-comment"][self.__filePath.split(".")[-1]],
                self.__config["finding-pattern"]["project"],
                self.__config["finding-pattern"]["validnumber"],
                self.__config["finding-pattern"]["placeholder"],
            ),
            findingText,
        ).end()
        # End position = end of line
        try:
            summary_end = re.search("\n", findingText).start()
        except AttributeError:
            summary_end = len(findingText)
        # Summary is text between start and end position
        summary = findingText[summary_start:summary_end]
        # Replace whitespaces with one whitespace
        summary = re.sub("\s+", " ", summary)
        # Delete whitespaces of left and right side
        return summary.strip()

    def __parseDescription(self, findingText):
        """
            Parse description by using finding text and delete comment-signs
            and whitespaces.

            description = second command line -> last commend line
        """

        # Start position = first postion of second line
        try:
            description_start = re.search("\n", findingText).end()
        except AttributeError:
            description_start = len(findingText)
        # Description text = start position -> end of finding text
        description = findingText[description_start:]
        # Replace line breaks with whitespace
        description = re.sub("\n", " ", description)
        # Replace command-signs with empty string
        description = re.sub(
            "{}\s*".format(
                self.__config["file-extension-comment"][self.__filePath.split(".")[-1]]
            ),
            "",
            description,
        )
        # Replace whitespaces with one whitespace
        description = re.sub("\s+", " ", description)
        # Delete whitespaces of left and right side
        return description.strip()

    def __parseComponents(self):
        """
            Parse components by using file path
        """
        if self.__modePython:
            return
        else:
            # Search MCAL into file path
            mcal = re.search("MCAL", self.__filePath)
            # Split path to get names of folder
            splitPath = self.__filePath.split(os.sep)
            if mcal:
                # With MCAL
                component = splitPath[-2] + "_" + splitPath[-3]
            else:
                # Without MCAL
                component = splitPath[-2]
            return {"name": component}

    def toDict(self):
        """
            Generate a dictionary with finding parameter
            If summary empty then return empty dict
        """

        if self.__summary != "":
            findingDict = {
                "project": {"key": self.__project},
                "summary": self.__summary,
                "description": self.__description,
                "issuetype": self.__issueType,
            }
            if not self.__modePython:
                findingDict.update({"components": [self.__components]})
            return findingDict
        else:
            return {}

    def getProject(self):
        """
            Return saved project
        """
        return self.__project

    def getId(self):
        """
            Return saved id
        """
        return self.__id

    def getPositionStart(self):
        """
            Return saved start position
        """
        return self.__posStart

    def setId(self, findingId):
        """
            Set findingId
        """
        if type(findingId) == int or type(findingId) == str:
            self.__id = str(findingId)

    def getSummary(self):
        """
            Get saved summary
        """
        return self.__summary

    def getDescription(self):
        """
            Get saved summary
        """
        return self.__description

    def __isDefinedFinding(self):
        """
            Check if finding is defined
        """
        return re.match(
            r"^({})$".format(self.__config["finding-pattern"]["validnumber"]), self.__id
        )

    def getIsDefined(self):
        """
            Get saved isDefined
        """
        return self.__isDefined
