import os
import yaml
import re
from SourceCode import log
from SourceCode import SourceFile


class SourceBase:
    """
        Search and get all files into a root-folder (base)
    """

    def __init__(self, rootPath, extensions, configFile, modePython):
        """
            Constructor - Create an object of class SourceBase
            and save object information
        """
        # Root-Path
        self.__rootPath = rootPath

        # Config
        self.__config = configFile

        # File extensions
        self.__extensions = extensions

        # Python mode
        self.__modePython = modePython

        # Search files
        self.__files = []

    def searchFiles(self):
        """
            Search all files with special extensions and save this into a list
        """
        # Decide between folder and file path
        if os.path.isfile(self.__rootPath):
            self.__addFile()
        else:
            # Search all files with special ending
            blacklistLoggingFolder = []
            for dirpath, _, files in os.walk(self.__rootPath):
                blacklistState, folderName = self.__isPathInBlacklist(dirpath)
                if not blacklistState:
                    for fileName in files:
                        # Generate SourceFile object and save this object into list
                        if (
                            fileName.split(".")[-1] in self.__extensions
                            and not blacklistState
                        ):
                            self.__createSourceBaseObject(dirpath + os.sep + fileName)
                else:
                    if folderName not in blacklistLoggingFolder:
                        blacklistLoggingFolder.append(folderName)
                        log.folderBlacklist(folderName, dirpath)

    def __addFile(self):
        """
            Save the file into a list
        """
        # Check is folder in blacklist
        blacklistState, folderName = self.__isPathInBlacklist(self.__rootPath)
        if not blacklistState:
            # Generate SourceFile object and save this object into list
            if self.__rootPath.split(".")[-1] in self.__extensions:
                self.__createSourceBaseObject(self.__rootPath)
        else:
            log.folderBlacklist(folderName, self.__rootPath)

    def __isPathInBlacklist(self, path):
        """
            Compare a path with a list of folder names into a blacklist
            Return True if path is blacklist else False
        """
        # Load folder blacklist
        folderBlacklist = yaml.safe_load(
            open(os.path.join(os.path.abspath(__file__), "..", "blacklist.yml"), "r")
        )

        # Blacklist folder?
        for folderName in folderBlacklist["FolderBlacklist"]:
            if re.search(folderName, path):
                return True, folderName
        else:
            return False, None

    def __createSourceBaseObject(self, path):
        """
            Create a SourceFile object and append this object in a list
        """
        try:
            sourceFile = SourceFile.SourceFile(path, self.__config, self.__modePython)
            sourceFile.readFileText()
            sourceFile.parseFindings()
            self.__files.append(sourceFile)
        except Exception as e:
            log.error(
                "Problem with file operation. The file {} is not longer treated.".format(
                    path
                ),
                e,
            )

    def getFiles(self):
        """
            Return all files in a list
        """
        return self.__files
